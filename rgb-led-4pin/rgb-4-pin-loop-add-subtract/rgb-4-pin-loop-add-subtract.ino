// RGB LED pins :  red  - ground - green - blue
int redPin = 7;
int greenPin = 6;
int bluePin = 5;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}
int n = 0, add_or_subtract = 1;
int count;

void loop() {
   setColor(n, 0, 0); 
   n = n + add_or_subtract;
   if(n == 255 || n == 0) add_or_subtract = add_or_subtract * -1;
   delay(10);//delay in milliseconds
   Serial.print(n);
   Serial.print(" ");
   count++;
  if (count % 20 == 0)Serial.println();
  
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
