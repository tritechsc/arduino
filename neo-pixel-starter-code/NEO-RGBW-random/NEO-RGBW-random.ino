
//RGBW demo cw coleman demo 2

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 6

#define NUM_LEDS 16

#define BRIGHTNESS 50

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);


void setup() {
  Serial.begin(115200);
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
  #if defined (__AVR_ATtiny85__)
    if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  #endif
  // End of trinket special code
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  randomSeed(analogRead(0));
}

//set up variables
int white = 0;
int i; 
long red,green, blue;
//the  loop starts
void loop() {
 for(i = 0; i < 16 ; i++){
          //    strip.setPixelColor(pin number , strip.Color( red value,green value , blue value ,white value ) );
          red = random(50,155);
          green = random(50,155);
          blue = random(50,155);
          white = random(10,20);
          strip.setPixelColor(i, strip.Color(red,green,blue,white ) );
       
          
 }
 strip.show();
delay (333);
 
}
