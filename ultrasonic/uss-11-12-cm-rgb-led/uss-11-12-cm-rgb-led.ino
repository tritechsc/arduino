
int redPin_7 = 7;
int greenPin_6 = 6;
int bluePin_5 = 5;

int redPin_4 = 4;
int greenPin_3 = 3;
int bluePin_2 = 2;
 
int trigPin = 11;    // Trigger
int echoPin = 12;    // Echo
long duration, cm, inches;
 
void setup() {
  //Serial Port begin
  Serial.begin (9600);
  //Define inputs and outputs
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  pinMode(redPin_7, OUTPUT);
  pinMode(greenPin_6, OUTPUT);
  pinMode(bluePin_5, OUTPUT);

    pinMode(redPin_4, OUTPUT);
  pinMode(greenPin_3, OUTPUT);
  pinMode(bluePin_2, OUTPUT);

  
}
 
void loop() {
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343

  

  Serial.print(cm);
  Serial.print(" cm");
  Serial.println();
  
  delay(250);

  // set the colors of the 2 RGB LEDS

 if(cm < 30){
    setColor1(200, 0, 200);
    setColor2(0, 200, 0);
  
 }else{
     setColor1(0, 0, 200);
    setColor2(200, 0, 0);

  
 }

  
}

void setColor1(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin_4, redValue);
  analogWrite(greenPin_3, greenValue);
  analogWrite(bluePin_2, blueValue);
}

void setColor2(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin_7, redValue);
  analogWrite(greenPin_6, greenValue);
  analogWrite(bluePin_5, blueValue);
}
